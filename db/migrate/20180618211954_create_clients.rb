class CreateClients < ActiveRecord::Migration[5.2]
  def change
    create_table :clients do |t|
      t.integer :chat_id
      t.string :ip
      t.string :token
      t.string :client_public
      t.string :server_private
      t.string :server_public
      t.string :refresh_token
      t.string :auth_token

      t.timestamps
    end
  end
end
