require 'telegram/bot'

class EchoController < ApplicationController
  before_action :check_ip

   def index
     data = params[:data]
     aes = OpenSSL::Cipher.new('AES-256-CBC')
     aes.encrypt

     aes.key = aes_key = aes.random_key
     aes.iv = aes_iv = aes.random_iv
     aes.padding = 1
     encrypted = Base64.encode64(aes.update(data) + aes.final)
     key = {}
     key[:key] = Base64.encode64(aes_key.to_s)
     key[:iv] = Base64.encode64(aes_iv.to_s)
     key = key.to_json

     rsa_key = OpenSSL::PKey::RSA.new @client[:client_public]
     key = rsa_key.public_encrypt(key, OpenSSL::PKey::RSA::PKCS1_OAEP_PADDING)

     answer = {}
     answer[:encrypted] = encrypted
     answer[:key] = Base64.encode64(key)

     render json: answer
   end

   def create
      rsa_key = OpenSSL::PKey::RSA.new @client[:server_private]
      aes = Base64.decode64(rsa_key.private_decrypt(Base64.decode64(params[:key])))
      aes = JSON.parse(aes)
      aes_key = Base64.decode64(aes['key'])
      aes_iv = Base64.decode64(aes['iv'])

      decipher = OpenSSL::Cipher::new('AES-256-CBC')
      decipher.decrypt
      decipher.key = aes_key
      decipher.iv = aes_iv
      decipher.padding = 1

      encrypted = Base64.decode64(params[:encrypted])
      decrypted = decipher.update(encrypted) + decipher.final

      render json: decrypted
   end

   def check_ip
     @client = Client.find_by auth_token: params[:auth_token]
     unless @client[:ip] != request.remote_ip
       unless @client[:refresh_token] == params[:refresh_token]
         p 'tokens not equals'
         token = '576808690:AAFy9pZFX2S80Qs4LgnJ6oeICdWZyVe9uLM'
         Telegram::Bot::Client.run(token) do |bot|
           bot.api.send_message(chat_id: @client[:chat_id], text: "Попытка несакционированного входа " + request.remote_ip)
         end
         render json: "error"
       end

       @client[:ip] = request.remote_ip
       @client.save
     end
   end
end