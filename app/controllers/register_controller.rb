require 'telegram/bot'

class RegisterController < ApplicationController
  def index
    render json: "working"
  end

  def create
    client = Client.find_by token: params[:code]
    unless client
      return render json: "error"
    end

    client_public_key = params[:public_key]
    key = OpenSSL::PKey::RSA.new(4096)
    public = key.public_key
    private = key

    length = 36;
    auth_token = rand(36 ** length).to_s(36)
    refresh_token = rand(36 ** length).to_s(36)

    client[:ip] = request.remote_ip
    client[:server_public] = public
    client[:token] = ''
    client[:client_public] = client_public_key
    client[:server_private] = private
    client[:auth_token] = auth_token
    client[:refresh_token] = refresh_token

    client.save

    response = {}
    response[:public] = Base64.encode64(public.to_s)
    response[:auth_token] = Base64.encode64(auth_token)
    response[:refresh_token] = Base64.encode64(refresh_token)

    token = '576808690:AAFy9pZFX2S80Qs4LgnJ6oeICdWZyVe9uLM'
    Telegram::Bot::Client.run(token) do |bot|
      bot.api.send_message(chat_id: client[:chat_id], text: "Ключи отправлены " + request.remote_ip)
      bot.api.send_message(chat_id: client[:chat_id], text: "Токен удален")
    end

    render json: response
  end
end